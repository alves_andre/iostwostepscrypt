//
//  iosTwoStepsCrypt
//  RSATest
//
//  Created by Andre Alves on 03/05/17.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonKeyDerivation.h>

@interface iosTwoStepsCrypt : NSObject
{
    SecKeyRef publicKey;
    SecKeyRef privateKey;
    NSData *publicTag;
    NSData *privateTag;
}

+ (instancetype)sharedInstance;

+ (NSString *)encryptedAESString:(NSString *)message withAESKey:(NSString *)AESKey;

+ (NSString *)decryptedAESString:(NSString *)message withAESKey:(NSString *)AESKey;

+ (NSString *)getAESKeyString;

- (NSString *)getPublicKey;

- (NSString *)getPrivateKey;

- (NSString *)encryptedRSAMessage:(NSString *)message withKey:(NSString *)key;

- (NSString *)decryptedRSAMessage:(NSString *)message;

@property (strong, nonatomic) NSString *AESKey;

@end

