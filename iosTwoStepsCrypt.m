//
//  iosTwoStepsCrypt.m
//  RSATest
//
//  Created by Andre Alves on 03/05/17.
//

#import "iosTwoStepsCrypt.h"

NSString * const
kCryptoError = @"ioasys.CryptManager";

const CCAlgorithm kAlgorithm = kCCAlgorithmAES128;
const NSUInteger kAlgorithmKeySize = kCCKeySizeAES128;
const NSUInteger kAlgorithmBlockSize = kCCBlockSizeAES128;
const NSUInteger kAlgorithmIVSize = kCCBlockSizeAES128;
const NSUInteger kPBKDFSaltSize = 8;
const NSUInteger kPBKDFRounds = 10000;  // ~80ms on an iPhone 4
const size_t BUFFER_SIZE = 64;
const size_t CIPHER_BUFFER_SIZE = 1024;
const uint32_t PADDING = kSecPaddingOAEP;

@implementation iosTwoStepsCrypt


+ (instancetype)sharedInstance{
    static ioasysTwoStepsCrypt *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [self new];
    });
    
    return sharedInstance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        NSString *privateKeyString = @"br.com.proconnectapp.iosapp.privatekey";
        NSString *publicKeyString = @"br.com.proconnectapp.iosapp.publickey";
        uint8_t privateKeyBuffer[100];
        memcpy(privateKeyBuffer, [privateKeyString UTF8String], [privateKeyString length]+1);
        uint8_t publicKeyBuffer[100];
        memcpy(publicKeyBuffer, [publicKeyString UTF8String], [publicKeyString length]+1);
        privateTag = [[NSData alloc] initWithBytes:privateKeyBuffer length:sizeof(privateKeyBuffer)];
        publicTag = [[NSData alloc] initWithBytes:publicKeyBuffer length:sizeof(publicKeyBuffer)];
    }
    return self;
}

+ (NSData *)randomDataOfLength:(size_t)length {
    NSMutableData *data = [NSMutableData dataWithLength:length];
    
    int result = SecRandomCopyBytes(kSecRandomDefault,
                                    length,
                                    data.mutableBytes);
    NSAssert(result == 0, @"Unable to generate random bytes: %d",
             errno);
    
    return data;
}

+ (NSData *)AESKeyForPassword:(NSString *)password salt:(NSData *)salt {
    NSMutableData *
    derivedKey = [NSMutableData dataWithLength:kAlgorithmKeySize];
    
    int
    result = CCKeyDerivationPBKDF(kCCPBKDF2,            // algorithm
                                  password.UTF8String,  // password
                                  [password lengthOfBytesUsingEncoding:NSUTF8StringEncoding],  // passwordLength
                                  salt.bytes,           // salt
                                  salt.length,          // saltLen
                                  kCCPRFHmacAlgSHA1,    // PRF
                                  kPBKDFRounds,         // rounds
                                  derivedKey.mutableBytes, // derivedKey
                                  derivedKey.length); // derivedKeyLen
    
    // Do not log password here
    NSAssert(result == kCCSuccess,
             @"Unable to create AES key for password: %d", result);
    
    return derivedKey;
}


+ (NSString *)encryptedMessageFromString:(NSString *)string password:(NSString *)password error:(NSError **)error{
    NSData *iv = [self randomDataOfLength:kAlgorithmIVSize];
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    size_t outLength;
    NSMutableData *
    cipherData = [NSMutableData dataWithLength:data.length +
                  kAlgorithmBlockSize];
    
    CCCryptorStatus
    result = CCCrypt(kCCEncrypt, // operation
                     kAlgorithm, // Algorithm
                     kCCOptionPKCS7Padding, // options
                     [password dataUsingEncoding:NSUTF8StringEncoding].bytes, // key
                     [password dataUsingEncoding:NSUTF8StringEncoding].length, // keylength
                     iv.bytes,// iv
                     data.bytes, // dataIn
                     data.length, // dataInLength,
                     cipherData.mutableBytes, // dataOut
                     cipherData.length, // dataOutAvailable
                     &outLength); // dataOutMoved
    
    if (result == kCCSuccess) {
        cipherData.length = outLength;
    }
    else {
        if (error) {
            *error = [NSError errorWithDomain:kCryptoError
                                         code:result
                                     userInfo:nil];
        }
        return nil;
    }
    
    return [cipherData base64EncodedStringWithOptions:0];
    
}
+ (NSString *)decryptMessageFromString:(NSString *)string password:(NSString *)password error:(NSError **)error{
    NSData *iv = [self randomDataOfLength:kAlgorithmIVSize];
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    size_t outLength;
    NSMutableData *
    cipherData = [NSMutableData dataWithLength:data.length +
                  kAlgorithmBlockSize];
    
    CCCryptorStatus
    result = CCCrypt(kCCDecrypt, // operation
                     kAlgorithm, // Algorithm
                     kCCOptionPKCS7Padding, // options
                     [password dataUsingEncoding:NSUTF8StringEncoding].bytes, // key
                     [password dataUsingEncoding:NSUTF8StringEncoding].length, // keylength
                     iv.bytes,// iv
                     data.bytes, // dataIn
                     data.length, // dataInLength,
                     cipherData.mutableBytes, // dataOut
                     cipherData.length, // dataOutAvailable
                     &outLength); // dataOutMoved
    
    if (result == kCCSuccess) {
        cipherData.length = outLength;
    }
    else {
        if (error) {
            *error = [NSError errorWithDomain:kCryptoError
                                         code:result
                                     userInfo:nil];
        }
        return nil;
    }
    
    return [cipherData base64EncodedStringWithOptions:0];
    
}

+ (NSString *)encryptedAESString:(NSString *)message withAESKey:(NSString *)AESKey{
    return [self encryptedMessageFromString:message password:AESKey error:nil];
}

+ (NSString *)decryptedAESString:(NSString *)message withAESKey:(NSString *)AESKey{
    return [self decryptMessageFromString:message password:AESKey error:nil];
}

+ (NSString *)getAESKeyString{
   NSData *salt = [self randomDataOfLength:16];
   return [[self AESKeyForPassword:[NSString stringWithFormat:@"%.0f",NSTimeIntervalSince1970] salt:salt] base64EncodedStringWithOptions:0];
}

- (NSData *)getKeyBitsFromKey:(SecKeyRef)givenKey {
    
    static const uint8_t publicKeyIdentifier[] = "com.your.company.publickey";
    NSData *newPublicTag = [[NSData alloc] initWithBytes:publicKeyIdentifier length:sizeof(publicKeyIdentifier)];
    
    OSStatus sanityCheck = noErr;
    NSData * publicKeyBits = nil;
    
    NSMutableDictionary * queryPublicKey = [[NSMutableDictionary alloc] init];
    [queryPublicKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [queryPublicKey setObject:newPublicTag forKey:(__bridge id)kSecAttrApplicationTag];
    [queryPublicKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    
    // Temporarily add key to the Keychain, return as data:
    NSMutableDictionary * attributes = [queryPublicKey mutableCopy];
    [attributes setObject:(__bridge id)givenKey forKey:(__bridge id)kSecValueRef];
    [attributes setObject:@YES forKey:(__bridge id)kSecReturnData];
    CFTypeRef result;
    sanityCheck = SecItemAdd((__bridge CFDictionaryRef) attributes, &result);
    if (sanityCheck == errSecSuccess) {
        publicKeyBits = CFBridgingRelease(result);
        
        // Remove from Keychain again:
        (void)SecItemDelete((__bridge CFDictionaryRef) queryPublicKey);
    }
    
    return publicKeyBits;
}

-(SecKeyRef)getPublicKeyRef {
    
    OSStatus sanityCheck = noErr;
    SecKeyRef publicKeyReference = NULL;
    
    if (publicKeyReference == NULL) {
        [self generateKeyPair:1024];
        NSMutableDictionary *queryPublicKey = [[NSMutableDictionary alloc] init];
        
        // Set the public key query dictionary.
        [queryPublicKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
        [queryPublicKey setObject:publicTag forKey:(__bridge id)kSecAttrApplicationTag];
        [queryPublicKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
        [queryPublicKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
        
        // Get the key.
        sanityCheck = SecItemCopyMatching((__bridge CFDictionaryRef)queryPublicKey, (CFTypeRef *)&publicKeyReference);
        
        
        if (sanityCheck != noErr)
        {
            publicKeyReference = NULL;
        }
        
        //        [queryPublicKey release];
        
    } else { publicKeyReference = publicKey; }
    
    return publicKeyReference;
}

- (void)generateKeyPair:(NSUInteger)keySize {
    OSStatus sanityCheck = noErr;
    publicKey = NULL;
    privateKey = NULL;
    
    //  LOGGING_FACILITY1( keySize == 512 || keySize == 1024 || keySize == 2048, @"%d is an invalid and unsupported key size.", keySize );
    
    // First delete current keys.
    //  [self deleteAsymmetricKeys];
    
    // Container dictionaries.
    NSMutableDictionary * privateKeyAttr = [[NSMutableDictionary alloc] init];
    NSMutableDictionary * publicKeyAttr = [[NSMutableDictionary alloc] init];
    NSMutableDictionary * keyPairAttr = [[NSMutableDictionary alloc] init];
    
    // Set top level dictionary for the keypair.
    [keyPairAttr setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [keyPairAttr setObject:[NSNumber numberWithUnsignedInteger:keySize] forKey:(__bridge id)kSecAttrKeySizeInBits];
    
    // Set the private key dictionary.
    [privateKeyAttr setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecAttrIsPermanent];
    [privateKeyAttr setObject:privateTag forKey:(__bridge id)kSecAttrApplicationTag];
    // See SecKey.h to set other flag values.
    
    // Set the public key dictionary.
    [publicKeyAttr setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecAttrIsPermanent];
    [publicKeyAttr setObject:publicTag forKey:(__bridge id)kSecAttrApplicationTag];
    // See SecKey.h to set other flag values.
    
    // Set attributes to top level dictionary.
    [keyPairAttr setObject:privateKeyAttr forKey:(__bridge id)kSecPrivateKeyAttrs];
    [keyPairAttr setObject:publicKeyAttr forKey:(__bridge id)kSecPublicKeyAttrs];
    
    // SecKeyGeneratePair returns the SecKeyRefs just for educational purposes.
    sanityCheck = SecKeyGeneratePair((__bridge CFDictionaryRef)keyPairAttr, &publicKey, &privateKey);
    if(sanityCheck == noErr  && publicKey != NULL && privateKey != NULL)
    {
        NSLog(@"Successful");
    }
}

- (SecKeyRef)getPrivateKeyRef {
    OSStatus resultCode = noErr;
    SecKeyRef privateKeyReference = NULL;
    [self generateKeyPair:1024];
    NSMutableDictionary * queryPrivateKey = [[NSMutableDictionary alloc] init];
    
    // Set the private key query dictionary.
    [queryPrivateKey setObject:(__bridge id)kSecClassKey forKey:(__bridge id)kSecClass];
    [queryPrivateKey setObject:privateTag forKey:(__bridge id)kSecAttrApplicationTag];
    [queryPrivateKey setObject:(__bridge id)kSecAttrKeyTypeRSA forKey:(__bridge id)kSecAttrKeyType];
    [queryPrivateKey setObject:[NSNumber numberWithBool:YES] forKey:(__bridge id)kSecReturnRef];
    
    // Get the key.
    resultCode = SecItemCopyMatching((__bridge CFDictionaryRef)queryPrivateKey, (CFTypeRef *)&privateKeyReference);
    NSLog(@"getPrivateKey: result code: %d", (int)resultCode);
    
    if(resultCode != noErr)
    {
        privateKeyReference = NULL;
    }

    return privateKeyReference;
}

- (NSString *)encryptStringWithPublicKey:(NSString *)publicKeyString andString:(NSString *)string{
    uint8_t *plainBuffer;
    uint8_t *cipherBuffer;
    
    unsigned long len = strlen([string UTF8String]);
    if (len > BUFFER_SIZE) len = BUFFER_SIZE-1;
    
    plainBuffer = (uint8_t *)calloc(BUFFER_SIZE, sizeof(uint8_t));
    cipherBuffer = (uint8_t *)calloc(CIPHER_BUFFER_SIZE, sizeof(uint8_t));
    
    strncpy( (char *)plainBuffer, [string UTF8String], len);
    return [self encryptWithCustomPublicKey:publicKeyString plainBuffer:plainBuffer cipherBuffer:cipherBuffer];
}

- (NSString *)decryptString:(NSString *)string{
    uint8_t *plainBuffer;
    uint8_t *cipherBuffer;
    
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:string options:0];
    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSASCIIStringEncoding];
    
    unsigned long len = strlen([decodedString UTF8String]);
    if (len > BUFFER_SIZE) len = BUFFER_SIZE-1;
    
    plainBuffer = (uint8_t *)calloc(BUFFER_SIZE, sizeof(uint8_t));
    cipherBuffer = (uint8_t *)calloc(CIPHER_BUFFER_SIZE, sizeof(uint8_t));
    
    strncpy( (char *)plainBuffer, [decodedString UTF8String], len);
    return [self decryptWithPrivateKey:cipherBuffer plainBuffer:plainBuffer];
}

- (NSString *)decryptWithPrivateKey:(uint8_t *)cipherBuffer plainBuffer:(uint8_t *)plainBuffer
{
    OSStatus status = noErr;
    
    size_t cipherBufferSize = strlen((char *)cipherBuffer);
    
    NSLog(@"decryptWithPrivateKey: length of buffer: %lu", BUFFER_SIZE);
    NSLog(@"decryptWithPrivateKey: length of input: %lu", cipherBufferSize);
    
    // DECRYPTION
    size_t plainBufferSize = BUFFER_SIZE;
    
    //  Error handling
    status = SecKeyDecrypt([self getPrivateKeyRef],
                           PADDING,
                           &cipherBuffer[0],
                           cipherBufferSize,
                           &plainBuffer[0],
                           &plainBufferSize
                           );
    NSLog(@"decryption result code: %d (size: %lu)", (int)status, plainBufferSize);
    NSLog(@"FINAL decrypted text: %s", plainBuffer);
    return [NSString stringWithUTF8String:(char *)plainBuffer];
}

- (NSString *)encryptWithCustomPublicKey:(NSString *)customPublicKey plainBuffer:(uint8_t *)plainBuffer cipherBuffer:(uint8_t *)cipherBuffer
{
    
    // NSLog(@"== encryptWithPublicKey()");
    
    OSStatus status = noErr;
    
    //NSLog(@"** original plain text 0: %s", plainBuffer);
    
    size_t plainBufferSize = strlen((char *)plainBuffer);
    size_t cipherBufferSize = CIPHER_BUFFER_SIZE;
    
    NSLog(@"SecKeyGetBlockSize() public = %lu", SecKeyGetBlockSize([self getPublicKeyRef]));
    //  Error handling
    // Encrypt using the public.NSString* plaintext = [dataObject JSONRepresentation];
    status = SecKeyEncrypt([self getPublicKeyRef],
                           PADDING,
                           plainBuffer,
                           plainBufferSize,
                           &cipherBuffer[0],
                           &cipherBufferSize
                           );
    //NSLog(@"encryption result code: %d (size: %lu)", (int)status, cipherBufferSize);
    //NSLog(@"encrypted text: %s", cipherBuffer);
    return  [[[NSData alloc] initWithBytes:cipherBuffer length:cipherBufferSize] base64EncodedStringWithOptions:0];
}


- (NSString *)encryptedRSAMessage:(NSString *)message withKey:(NSString *)key{
    return [self encryptStringWithPublicKey:key andString:message];
}

- (NSString *)decryptedRSAMessage:(NSString *)message{
    return [self decryptString:message];
}

- (NSString *)getPublicKey{
    //    size_t keySize = SecKeyGetBlockSize([self getPublicKeyRef]);
    //    NSData *keyData = [NSData dataWithBytes:[self getPublicKeyRef] length:keySize];
    return  [[self getKeyBitsFromKey:[self getPublicKeyRef]] base64EncodedStringWithOptions:0];
}

- (NSString *)getPrivateKey{
    //    size_t keySize = SecKeyGetBlockSize([self getPrivateKeyRef]);
    //    NSData *keyData = [NSData dataWithBytes:[self getPrivateKeyRef] length:keySize];
    return  [[self getKeyBitsFromKey:[self getPrivateKeyRef]] base64EncodedStringWithOptions:0];
}


@end
